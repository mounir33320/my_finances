<?php

namespace App\FileBankManager;

use App\Entity\OperationFile;
use App\Entity\User;
use App\Exception\FileBankManagerNotFoundException;
use phpDocumentor\Reflection\Types\True_;
use Symfony\Component\DependencyInjection\Attribute\TaggedIterator;

class FileBankManagerContext
{
    /**
     * @var FileBankManagerInterface[]|iterable
     */
    private iterable $fileBankManagers;

    public function __construct(
        #[TaggedIterator('app.filebank_manager')]
        iterable $fileBankManagers
    )
    {
        $this->fileBankManagers = $fileBankManagers;
    }

    public function manageFile(OperationFile $operationFile, User $user): bool
    {
        foreach ($this->fileBankManagers as $fileBankManager) {
            if ($fileBankManager->supports($operationFile->getBankName())) {
                $fileBankManager->manageFile($operationFile, $user);

                return true;
            }
        }

        throw new FileBankManagerNotFoundException('Aucun manager n\'a été trouvé');
    }
}
