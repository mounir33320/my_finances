<?php

namespace App\Model;

use App\Entity\Charge;

class ChargesInfo
{
    public ?float $salary = 0;

    public ?float $totalAmount = 0;

    public ?float $remainingSalary = 0;

    public ?float $remainingToBePaidThisMonth;

    public ?float $perDay = 0;

    /** @var Charge[] $charges */
    public function __construct(array $charges)
    {
        if (!empty($charges)) {
            $this->salary = $charges[0]->getUser()->getSalary();

            $this->totalAmount = array_reduce($charges, function ($carry, Charge $charge) {
               $carry += $charge->getAmount();
               return $carry;
            });

            $this->remainingToBePaidThisMonth = array_reduce($charges, function($carry, Charge $charge) {
                if ($charge->getDirectDebitDay() !== 'NA' && (int) $charge->getDirectDebitDay() > (new \DateTime())->format('d') ) {
                   $carry += $charge->getAmount();
               }

               return $carry;
            }) ?? 0;

            $this->remainingSalary = $this->salary - $this->totalAmount;
            $nbDaysInMonth = (int) (new \DateTime())->modify('last day of this month')->format('d');
            $this->perDay = $this->remainingSalary / $nbDaysInMonth;
        }
    }
}
