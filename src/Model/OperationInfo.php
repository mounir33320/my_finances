<?php

namespace App\Model;

class OperationInfo
{
    public float $totalDebit = 0;
    public string $totalDebitFormatted = '';

    public float $totalCredit = 0;
    public string $totalCreditFormatted = '';

    public float $difference = 0;
    public string $differenceFormatted = '';
    public string $differenceBadgeClass = '';


}
