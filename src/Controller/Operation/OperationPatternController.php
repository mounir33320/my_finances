<?php

namespace App\Controller\Operation;

use App\Entity\OperationPattern;
use App\Form\OperationType\OperationPatternType;
use App\Repository\OperationRepository;
use App\Service\OperationService\OperationPatternService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/operation-patterns', name: 'operation-patterns_')]
class OperationPatternController extends AbstractController
{
    public function __construct(private readonly OperationPatternService $operationPatternService, private readonly OperationRepository $operationRepository)
    {
    }

    #[Route('/', name: 'index')]
    public function index(): RedirectResponse|Response
    {
        $patterns = $this->operationPatternService->getPatternsByUser($this->getUser());

        return $this->render('operations/operation_pattern/index.html.twig', [
            'patterns' => $patterns,
        ]);
    }

    #[Route('/add/', name: 'add')]
    public function add(Request $request): RedirectResponse|Response
    {
        $pattern = new OperationPattern();
        $form = $this->createForm(OperationPatternType::class, $pattern, ['action' => $request->getRequestUri()]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->operationPatternService->save($pattern, $this->getUser());
            $this->operationRepository->updateCategoriesByPatternAndUser($pattern, $this->getUser());
            $this->addFlash('success', 'Pattern ajouté avec succès');

            return $this->redirectToRoute('operation-patterns_index');
        }

        return $this->render('operations/operation_pattern/form.html.twig', [
            'form' => $form->createView(),
            'title' => 'Ajouter un pattern',
            'submitButtonName' => 'Ajouter'
        ]);
    }

    #[Route('/edit/{id}', name: 'edit')]
    public function edit(OperationPattern $pattern, Request $request): RedirectResponse|Response
    {
        $form = $this->createForm(OperationPatternType::class, $pattern, ['action' => $request->getRequestUri()]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->operationPatternService->update($pattern);
            $this->addFlash('success', 'Pattern modifié avec succès');

            return $this->redirectToRoute('operation-patterns_index');
        }

        return $this->render('operations/operation_pattern/form.html.twig', [
            'form' => $form->createView(),
            'title' => 'Éditer un pattern',
            'submitButtonName' => 'Modifier'
        ]);
    }

    #[Route('/delete/{id}', name: 'delete')]
    public function delete(Request $request, OperationPattern $pattern): RedirectResponse
    {
        $token = $request->get('_token');

        if ($this->isCsrfTokenValid('delete'.$pattern->getId(),$token)) {
            $this->operationPatternService->delete($pattern);
            $this->addFlash('success', 'Pattern supprimé avec succès');
        } else {
            $this->addFlash('error', 'Une erreur est survenue');
        }

        return $this->redirect($request->headers->get('referer'));
    }
}
