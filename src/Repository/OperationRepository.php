<?php

namespace App\Repository;

use App\Entity\Operation;
use App\Entity\OperationPattern;
use App\Entity\User;
use App\Model\OperationFilter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @extends ServiceEntityRepository<Operation>
 *
 * @method Operation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Operation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Operation[]    findAll()
 * @method Operation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OperationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Operation::class);
    }

    public function findByFilter(OperationFilter $filter, UserInterface $user)
    {
        $parameters = [];

        $qb = $this->createQueryBuilder('o');
        $qb->andWhere('o.user = :user');
        $parameters['user'] = $user;

        if ($filter->getDateFrom()) {
            $qb->andWhere('o.date > :dateFrom');
            $parameters['dateFrom'] = $filter->getDateFrom();
        }

        if ($filter->getDateTo()) {
            $qb->andWhere('o.date < :dateTo');
            $parameters['dateTo'] = $filter->getDateTo();
        }

        if ($filter->getCategories()->count() > 0) {
            $k = 0;

            $queryString = '';
            foreach ($filter->getCategories() as $category) {
                $queryString .= 'o.category = :category'.$k. ' OR ';
                $parameters['category'.$k] = $category;
                $k++;
            }
            $queryString = preg_replace('/ ?OR ?$/', '', $queryString);

            $qb->andWhere($queryString);
        }

        $qb->setParameters($parameters);

        $qb->addOrderBy('o.date', 'DESC');

        return $qb->getQuery()->getResult();
    }

    public function save(Operation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Operation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function deleteAllOperation(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new \Exception('Une instance de la classe User est attendue');
        }

        return $this->createQueryBuilder('o')
            ->delete()
            ->where('o.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult()
        ;
    }

    public function update(Operation $operation): void
    {
        $this->_em->flush();
    }

    public function flush(): void
    {
        $this->_em->flush();
    }

    public function updateCategoriesByPatternAndUser(OperationPattern $pattern, UserInterface $user)
    {
        $queryBuilder = $this->createQueryBuilder('o');
        $query = $queryBuilder->update()
            ->set('o.category', ':category')
            ->andWhere('LOWER(o.label) LIKE :label')
            ->andWhere('o.user = :user')
            ->setParameters([
                'category' => $pattern->getCategory(),
                'label' => "%".strtolower($pattern->getPattern())."%",
                'user' => $user
            ])
            ->getQuery();
        $query->execute();
    }
}
