<?php

namespace App\Controller;

use App\Entity\Debt;
use App\Entity\Payment;
use App\Form\DebtType;
use App\Form\PaymentType;
use App\Service\DebtService;
use App\Service\PaymentService;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/debts', name: 'debts_')]
class DebtController extends AbstractController
{
    public function __construct(private readonly DebtService $debtService, private readonly PaymentService $paymentService)
    {
    }

    /**
     * @throws \Exception
     */
    #[Route('/', name: 'index')]
    public function index(): Response
    {
        $debtListDTO = $this->debtService->getDebtListByUser($this->getUser());

        return $this->render('debt/index.html.twig', [
            'debtsList' => $debtListDTO
        ]);
    }

    /**
     * @throws \Exception
     */
    #[Route('/add', name: 'add')]
    public function create(Request $request): RedirectResponse|Response
    {
        $debt = new Debt();
        $form = $this->createForm(DebtType::class, $debt);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->debtService->create($this->getUser(), $debt);
            $this->addFlash('success', 'Dette créée avec succès');

            return $this->redirectToRoute('debts_index');
        }

        return $this->render('debt/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    #[Route('/{id}', name: 'show')]
    public function show(Debt $debt, Request $request): Response | RedirectResponse
    {
        $debtDto = $this->debtService->convertDebtToDto($debt);

        return $this->render('debt/show.html.twig', [
            'debt' => $debtDto,
        ]);
    }

    #[Route('/{id}/compute-remaining-months', name: 'compute-remaining-months')]
    public function computeRemainingMonths(Debt $debt): Response
    {
        $debtDto = $this->debtService->convertDebtToDto($debt);

        return $this->render('debt/compute-remaining-month.html.twig', ['debt' => $debtDto]);
    }

    #[Route('/delete/{id}', name: 'delete')]
    public function delete(Debt $debt, Request $request): RedirectResponse
    {
        $token = $request->get('_token');

        if ($this->isCsrfTokenValid('delete'.$debt->getId(),$token)) {
            $this->debtService->delete($debt);
            $this->addFlash('success', 'Dette supprimée avec succès');
            return $this->redirectToRoute('debts_index');
        }

        return $this->redirectToRoute('debts_index');
    }
}
