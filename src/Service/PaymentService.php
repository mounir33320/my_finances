<?php

namespace App\Service;

use App\Entity\Debt;
use App\Entity\Payment;
use App\Repository\PaymentRepository;
use Doctrine\ORM\EntityManagerInterface;

class PaymentService
{
    public function __construct(private readonly PaymentRepository $paymentRepository)
    {
    }

    public function save(Payment $payment, Debt $debt): Payment
    {
        $payment->getCreatedAt() ?? $payment->setCreatedAt(new \DateTimeImmutable());
        $debt->addPayment($payment);
        $this->paymentRepository->save($payment, true);

        return $payment;
    }

    public function update(Payment $payment): Payment
    {
        $this->paymentRepository->update($payment);

        return $payment;
    }

    public function delete(Payment $payment): void
    {
        $this->paymentRepository->remove($payment, true);
    }
}
