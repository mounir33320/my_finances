<?php

namespace App\Repository;

use App\Entity\OperationPattern;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<OperationPattern>
 *
 * @method OperationPattern|null find($id, $lockMode = null, $lockVersion = null)
 * @method OperationPattern|null findOneBy(array $criteria, array $orderBy = null)
 * @method OperationPattern[]    findAll()
 * @method OperationPattern[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OperationPatternRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OperationPattern::class);
    }

    public function save(OperationPattern $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(OperationPattern $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function update(OperationPattern $pattern): void
    {
        $this->_em->flush();
    }

}
