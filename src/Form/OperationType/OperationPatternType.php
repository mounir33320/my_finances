<?php

namespace App\Form\OperationType;

use App\Entity\OperationCategory;
use App\Entity\OperationPattern;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class OperationPatternType extends AbstractType
{
    public function __construct(private readonly TokenStorageInterface $storage)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('pattern', TextType::class, [
                'label' => 'Pattern à rechercher',
                'required' => true
            ])
            ->add('category', EntityType::class, [
                'class' => OperationCategory::class,
                'choice_label' => 'name',
                'label' => 'Catégorie',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->andWhere('c.user = ' . $this->storage->getToken()->getUser()->getId())
                        ;
                },
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => OperationPattern::class,
        ]);
    }
}
