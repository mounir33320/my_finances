import {Notyf} from "notyf";
import "notyf/notyf.min.css"

let toastElts = document.querySelectorAll(".toast-notification");

if(toastElts){
    //Custom toast
    const notyf = new Notyf({
        types: [
            {
                type: 'warning',
                className: "bg-warning",
                icon: "<i class=\"fas fa-exclamation-triangle\"></i>"
            }
        ]
    });

    toastElts.forEach(element => {
        notyf.open({
            type: element.dataset.type,
            message: element.dataset.message,
            position: {
                x: "right",
                y: "top"
            },
            duration: 6000
        });
    })
}

export const displayToast = (type, message) => {
    const notyf = new Notyf();
    notyf.open({
        type: type,
        message: message,
        position: {
            x: "right",
            y: "top"
        },
        duration: 6000
    })
}
