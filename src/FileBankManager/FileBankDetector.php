<?php

namespace App\FileBankManager;

use App\CsvReader\CreditAgricoleCsvReader;
use App\Entity\OperationFile;
use App\Exception\FileBankNotFoundException;

class FileBankDetector
{
    public function __construct(private readonly CreditAgricoleCsvReader $creditAgricoleCsvReader)
    {
    }

    /**
     * @throws FileBankNotFoundException
     */
    public function detectAndGetBankName(OperationFile $operationFile): string
    {
        if ($this->isCreditAgricole($operationFile)) {
            return CreditAgricoleFileManager::BANK_NAME;
        }

        throw new FileBankNotFoundException('Le fichier uploadé ne correspond pas');
    }

    private function isCreditAgricole(OperationFile $operationFile): bool
    {
        $expectedHeader = ['date', 'libelle', 'debit_euros', 'credit_euros'];
        $header = $this->creditAgricoleCsvReader->getHeader($operationFile);

        return $expectedHeader === $header;
    }
}
