<?php

namespace App\Service\OperationService;

use App\Entity\Operation;
use App\Entity\OperationFile;
use App\Entity\OperationPattern;
use App\Entity\User;
use App\Exception\FileBankNotFoundException;
use App\FileBankManager\FileBankDetector;
use App\FileBankManager\FileBankManagerContext;
use App\Repository\OperationFileRepository;
use App\Repository\OperationPatternRepository;
use App\Repository\OperationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\User\UserInterface;

class OperationFileService
{
    public function __construct(
        private readonly ContainerBagInterface $params,
        private readonly OperationFileRepository    $operationFileRepository)
    {
    }

    public function save(UploadedFile $file, User $user): OperationFile
    {
        $filename = md5(microtime()) . '_' . (new \DateTime())->format('d-m-Y-H-i-s');
        $directory = $this->params->get('app.media_path').'/operation-files';
        $file->move($directory, $filename);
        $operationFile = (new OperationFile())
            ->setFilename($filename)
            ->setFilePath($directory.'/'.$filename)
            ->setUser($user)
        ;
        $this->operationFileRepository->save($operationFile, true);

        return $operationFile;
    }
}
