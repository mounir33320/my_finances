<?php

namespace App\DTO;

use App\Entity\Payment;
use Doctrine\Common\Collections\Collection;

class DebtDto
{
    public float $amount = 0;
    public float $amountPercentAboutAllDebts = 0;
    public string $creditor = '';
    public array $payments = [];
    public float $remainingAmount = 0;
    public float $remainingAmountPercent = 0;
    public float $alreadyPaid = 0;
    public float $alreadyPaidPercent = 0;
    public string $url = '';
    public string $removeUrl;
    public int $id;
}
