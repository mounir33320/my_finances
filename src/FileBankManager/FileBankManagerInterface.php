<?php

namespace App\FileBankManager;

use App\Entity\OperationFile;
use App\Entity\User;

interface FileBankManagerInterface
{
    /**
     * @param OperationFile $operationFile
     * @param User $user
     * @return void
     */
    public function manageFile(OperationFile $operationFile, User $user): void;

    public function supports(string $bankName): bool;
}
