export class MounDatatable{
    constructor(element, options) {
        this.element = element;
        this.options = options;
        this.init();
    }

    init() {
        for (const [key, value] of Object.entries(this.options)) {
            console.log(value === true)
            switch (key) {
                case 'searchable':
                    if (value === true) this.displaySearchInput()
                break;

                case 'pagination' :
                    if (value === true) this.displayPagination()
            }
        }
    }

    displaySearchInput() {
        const inputContainer = document.createElement('div');
        inputContainer.className = 'my-2'

        const inputSearch = document.createElement('input');
        inputSearch.className = 'form-control';
        inputSearch.autocomplete = 'off';
        inputSearch.placeholder = 'Rechercher...';
        inputSearch.style.maxWidth = '150px';

        inputContainer.appendChild(inputSearch);
        this.element.parentNode.insertBefore(inputContainer, this.element);
    }

    displayPagination() {
        const trElements = this.element.querySelectorAll('tbody tr')
        let pages = Math.ceil(trElements.length / 10);
        console.log(pages)

        let trElementsPaginated = {};

        let pageNumber = 1;
        let k = 1;
        for (let i = 0; i < trElements.length; i++) {
            if (k === 10) {
                k = 0;
                pageNumber++;
            }
            if (trElementsPaginated[pageNumber] === undefined) {
                trElementsPaginated[pageNumber] = [];
            }
            trElementsPaginated[pageNumber].push(trElements[i]);

            k++;
        }

        const tbodyElement = this.element.querySelector('tbody');
        tbodyElement.innerHTML = '';

        trElementsPaginated[1].forEach(element => {
            tbodyElement.appendChild(element);
        })

        const paginationContainer = document.createElement('div');
    }
}
