<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230804172542 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE operation_payment_type DROP FOREIGN KEY FK_44C64951A76ED395');
        $this->addSql('ALTER TABLE operation_payment_type_pattern DROP FOREIGN KEY FK_F529DD21A76ED395');
        $this->addSql('DROP TABLE operation_payment_type');
        $this->addSql('DROP TABLE operation_payment_type_pattern');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE operation_payment_type (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, payment_type VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, icon VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, INDEX IDX_44C64951A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE operation_payment_type_pattern (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, payment_type_pattern VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, INDEX IDX_F529DD21A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE operation_payment_type ADD CONSTRAINT FK_44C64951A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE operation_payment_type_pattern ADD CONSTRAINT FK_F529DD21A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }
}
