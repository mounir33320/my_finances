<?php

namespace App\Service\OperationService;

use App\Entity\Operation;
use App\Entity\OperationFile;
use App\Entity\User;
use App\Exception\FileBankManagerNotFoundException;
use App\Exception\FileBankNotFoundException;
use App\FileBankManager\CreditAgricoleFileManager;
use App\FileBankManager\FileBankDetector;
use App\FileBankManager\FileBankManagerContext;
use App\FileBankManager\FileBankManagerInterface;
use App\Model\OperationFilter;
use App\Model\OperationInfo;
use App\Repository\OperationRepository;
use Symfony\Component\Security\Core\User\UserInterface;

class OperationService
{
    public function __construct(
        private readonly OperationRepository $operationRepository,
        private readonly FileBankDetector $fileBankDetector,
        private readonly FileBankManagerContext $fileBankManagerContext){}

    public function getAll(OperationFilter $filter, UserInterface $user): array
    {
        return $this->operationRepository->findByFilter($filter, $user);
    }

    public function getOperationInfo(array $operations): OperationInfo
    {
        $totalDebit = array_reduce($operations, function ($carry, Operation $operation) {
            if ($operation->getAmount() < 0) {
                $carry += $operation->getAmount();
            }

            return $carry;
        });

        $totalCredit = array_reduce($operations, function ($carry, Operation $operation) {
            if ($operation->getAmount() > 0) {
                $carry += $operation->getAmount();
            }

            return $carry;
        });

        $operationInfo = new OperationInfo();
        $operationInfo->totalDebit = $totalDebit ?? 0;
        $operationInfo->totalDebitFormatted = number_format($totalDebit, 2, ',', ' ');

        $operationInfo->totalCredit = $totalCredit ?? 0;
        $operationInfo->totalCreditFormatted = number_format($totalCredit, 2, ',', ' ');

        $operationInfo->difference = $totalCredit + $totalDebit;
        $operationInfo->differenceFormatted = number_format($operationInfo->difference, 2, ',', ' ');

        $operationInfo->differenceBadgeClass = $operationInfo->difference !== 0 ? ($operationInfo->difference > 0 ? 'badge bg-success' : 'badge bg-danger') : 'badge bg-secondary';

        return $operationInfo;
    }

    public function update(Operation $operation): Operation
    {
        $this->operationRepository->update($operation);

        return $operation;
    }

    public function deleteAllOperations(User $user): void
    {
        $this->operationRepository->deleteAllOperation($user);
    }

    /**
     * @throws FileBankNotFoundException
     * @throws FileBankManagerNotFoundException
     */
    public function saveOperationsByOperationFile(OperationFile $operationFile, User $user): void
    {
        $bankName = $this->fileBankDetector->detectAndGetBankName($operationFile);
        $operationFile->setBankName($bankName);
        $this->fileBankManagerContext->manageFile($operationFile, $user);
    }
}
