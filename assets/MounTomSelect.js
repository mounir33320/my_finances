import TomSelect from "tom-select";

export class MounTomSelect {
    constructor(selectElements) {
        this.selectElements = selectElements;

        this.init();
    }

    init() {
        if (this.selectElements) {
            this.selectElements.forEach(select => {
                let config = {
                    allowEmptyOption: true,
                    plugins: ['dropdown_input'],
                    closeAfterSelect: true
                }

                if(select.multiple){
                    config.plugins.push('remove_button');
                    config = { hideSelected: true, ...config }
                }

                new TomSelect(select,config);
            })
        }
    }
}
