#syntax=docker/dockerfile:1.4
FROM php:8.2-cli as php_build

ARG APP_ENV="prod"
ARG DATABASE_URL="sqlite:///mydb.db"

ENV APP_ENV=${APP_ENV}
ENV DATABASE_URL=${DATABASE_URL}

WORKDIR /app

RUN apt update -y && apt install -y $PHPIZE_DEPS zip

COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/

RUN set -eux; \
    install-php-extensions zip

COPY . .
COPY .??* ./

# Installer Composer
COPY --from=composer:2.6.6 /usr/bin/composer /usr/bin/composer

RUN if [ "$APP_ENV" = "prod" ]; then \
      composer install --no-dev --optimize-autoloader; \
    else \
      composer install --optimize-autoloader;\
    fi


FROM node:22.13.1-slim as js_build

WORKDIR /app

COPY package.json yarn.lock webpack.config.js ./
COPY assets ./assets

RUN yarn install && yarn run build

FROM php:8.2-fpm-alpine as base
WORKDIR /app

COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/

RUN set -eux; \
    install-php-extensions intl \
    pdo_mysql \
    && adduser -D -u 1000 appuser \
    && addgroup -g 101 nginx  \
    && adduser appuser nginx

COPY --from=php_build /app /app
COPY --from=js_build /app/public/build /app/public/build

RUN mkdir /var/run/php && chown -R appuser:nginx /var/run/php && chown -R appuser:nginx /app

COPY --link docker/php/php-fpm.d/zz-docker.conf /usr/local/etc/php-fpm.d/zz-docker.conf

FROM base as dev

# Installer Composer
COPY --from=composer:2.6.6 /usr/bin/composer /usr/bin/composer

RUN apk add --no-cache nodejs=22.13.1-r0 npm && npm i -g yarn

RUN set -eux; \
    install-php-extensions xdebug \
    && apk add --no-cache bash \
    && curl -1sLf 'https://dl.cloudsmith.io/public/symfony/stable/setup.alpine.sh' | bash \
    && apk add symfony-cli

COPY --link docker/php/php-fpm.d/xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini


USER appuser

CMD ["symfony", "serve", "--listen-ip=0.0.0.0", "--port=8000"]

FROM base as prod
USER appuser
