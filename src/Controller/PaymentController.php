<?php

namespace App\Controller;

use App\Entity\Debt;
use App\Entity\Payment;
use App\Form\PaymentType;
use App\Service\PaymentService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/payments', name: 'payments_')]
class PaymentController extends AbstractController
{
    public function __construct(private readonly PaymentService $paymentService)
    {
    }

    #[Route('/add/debt/{id}', name: 'add', methods: ['GET', 'POST'])]
    public function add(Request $request, Debt $debt): RedirectResponse|Response
    {
        $payment = new Payment();
        $form = $this->createForm(PaymentType::class, $payment, ['action' => $request->getRequestUri()]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->paymentService->save($payment, $debt);
            $this->addFlash('success', 'Paiement ajouté avec succès');

            return $this->redirectToRoute('debts_show', ['id' => $payment->getDebt()->getId()]);
        }

        return $this->render('payment/form.html.twig', [
            'form' => $form->createView(),
            'title' => 'Ajouter un paiement',
            'submitButtonName' => 'Ajouter'
        ]);
    }

    #[Route('/edit/{id}', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(Payment $payment, Request $request): RedirectResponse|Response
    {
        $form = $this->createForm(PaymentType::class, $payment, ['action' => $request->getRequestUri()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
           $this->paymentService->update($payment);
           $this->addFlash('success', 'Paiement modifié avec succès');

           return $this->redirectToRoute('debts_show', ['id' => $payment->getDebt()->getId()]);
        }

        return $this->render('payment/form.html.twig', [
            'form' => $form->createView(),
            'title' => 'Modifier le paiement',
            'submitButtonName' => 'Modifier'
        ]);
    }

    #[Route('delete/{id}', name: 'delete')]
    public function delete(Request $request, Payment $payment): RedirectResponse
    {
        $token = $request->get('_token');

        if ($this->isCsrfTokenValid('delete'.$payment->getId(),$token)) {
            $this->paymentService->delete($payment);
            $this->addFlash('success', 'Paiement supprimé avec succès');
            return $this->redirect($request->headers->get('referer'));
        }

        $this->addFlash('error', 'Une erreur est survenue');
        return $this->redirect($request->headers->get('referer'));
    }
}
