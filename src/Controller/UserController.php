<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserSalaryType;
use App\Form\UserType;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('user', name: 'user_')]
class UserController extends AbstractController
{
    public function __construct(private readonly UserService $userService)
    {
    }

    #[Route('/update/{id}', name: 'update')]
    public function update(User $user, Request $request): RedirectResponse|Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->userService->update($user);
            $this->addFlash('success', 'Le mot de passe a bien été modifié !');
            return $this->redirectToRoute('home_index');
        }

        return $this->render('account/update.html.twig', [
            'form' => $form->createView()
        ]);
    }

    #[Route('/update-salary/{id}', name: 'update_salary')]
    public function updateSalary(User $user, Request $request): RedirectResponse|Response
    {
        $form = $this->createForm(UserSalaryType::class, $user, ['action' => $request->getRequestUri()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->userService->updateSalary($user);
            $this->addFlash('success', 'Le salaire a bien été modifié !');
            return $this->redirectToRoute('charge_index');
        }

        return $this->render('user/salary_form.html.twig', [
            'form' => $form->createView(),
            'title' => 'Modifier mon salaire',
            'submitButtonName' => 'Modifier'
        ]);
    }
}
