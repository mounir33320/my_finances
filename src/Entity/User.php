<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 180, unique: true)]
    private ?string $username = null;

    #[ORM\Column]
    private array $roles = [];

    /**
     * @var string The hashed password
     */
    #[ORM\Column]
    private ?string $password = null;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Debt::class, orphanRemoval: true)]
    private Collection $debts;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Charge::class, orphanRemoval: true)]
    private Collection $charges;

    #[ORM\Column]
    private ?float $salary = 0;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Operation::class, orphanRemoval: true)]
    private Collection $operations;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: OperationCategory::class, orphanRemoval: true)]
    private Collection $operationCategories;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: OperationPattern::class, orphanRemoval: true)]
    private Collection $operationPatterns;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: OperationFile::class, orphanRemoval: true)]
    private Collection $operationFiles;

    public function __construct()
    {
        $this->debts = new ArrayCollection();
        $this->charges = new ArrayCollection();
        $this->operations = new ArrayCollection();
        $this->operationCategories = new ArrayCollection();
        $this->operationPatterns = new ArrayCollection();
        $this->operationFiles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->username;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection<int, Debt>
     */
    public function getDebts(): Collection
    {
        return $this->debts;
    }

    public function addDebt(Debt $debt): self
    {
        if (!$this->debts->contains($debt)) {
            $this->debts->add($debt);
            $debt->setUser($this);
        }

        return $this;
    }

    public function removeDebt(Debt $debt): self
    {
        if ($this->debts->removeElement($debt)) {
            // set the owning side to null (unless already changed)
            if ($debt->getUser() === $this) {
                $debt->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Charge>
     */
    public function getCharges(): Collection
    {
        return $this->charges;
    }

    public function addCharge(Charge $charge): self
    {
        if (!$this->charges->contains($charge)) {
            $this->charges->add($charge);
            $charge->setUser($this);
        }

        return $this;
    }

    public function removeCharge(Charge $charge): self
    {
        if ($this->charges->removeElement($charge)) {
            // set the owning side to null (unless already changed)
            if ($charge->getUser() === $this) {
                $charge->setUser(null);
            }
        }

        return $this;
    }

    public function getSalary(): ?float
    {
        return $this->salary;
    }

    public function setSalary(float $salary): self
    {
        $this->salary = $salary;

        return $this;
    }

    /**
     * @return Collection<int, Operation>
     */
    public function getOperations(): Collection
    {
        return $this->operations;
    }

    public function addOperation(Operation $operation): self
    {
        if (!$this->operations->contains($operation)) {
            $this->operations->add($operation);
            $operation->setUser($this);
        }

        return $this;
    }

    public function removeOperation(Operation $operation): self
    {
        if ($this->operations->removeElement($operation)) {
            // set the owning side to null (unless already changed)
            if ($operation->getUser() === $this) {
                $operation->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, OperationCategory>
     */
    public function getOperationCategories(): Collection
    {
        return $this->operationCategories;
    }

    public function addOperationCategory(OperationCategory $operationCategory): self
    {
        if (!$this->operationCategories->contains($operationCategory)) {
            $this->operationCategories->add($operationCategory);
            $operationCategory->setUser($this);
        }

        return $this;
    }

    public function removeOperationCategory(OperationCategory $operationCategory): self
    {
        if ($this->operationCategories->removeElement($operationCategory)) {
            // set the owning side to null (unless already changed)
            if ($operationCategory->getUser() === $this) {
                $operationCategory->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, OperationPattern>
     */
    public function getOperationPatterns(): Collection
    {
        return $this->operationPatterns;
    }

    public function addOperationPattern(OperationPattern $operationPattern): self
    {
        if (!$this->operationPatterns->contains($operationPattern)) {
            $this->operationPatterns->add($operationPattern);
            $operationPattern->setUser($this);
        }

        return $this;
    }

    public function removeOperationPattern(OperationPattern $operationPattern): self
    {
        if ($this->operationPatterns->removeElement($operationPattern)) {
            // set the owning side to null (unless already changed)
            if ($operationPattern->getUser() === $this) {
                $operationPattern->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, OperationFile>
     */
    public function getOperationFiles(): Collection
    {
        return $this->operationFiles;
    }

    public function addOperationFile(OperationFile $operationFile): self
    {
        if (!$this->operationFiles->contains($operationFile)) {
            $this->operationFiles->add($operationFile);
            $operationFile->setUser($this);
        }

        return $this;
    }

    public function removeOperationFile(OperationFile $operationFile): self
    {
        if ($this->operationFiles->removeElement($operationFile)) {
            // set the owning side to null (unless already changed)
            if ($operationFile->getUser() === $this) {
                $operationFile->setUser(null);
            }
        }

        return $this;
    }
}
