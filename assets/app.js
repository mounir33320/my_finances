import './styles/app.scss';

import 'fontawesome-free/js/all.min';
import './toast'
import TomSelect from "tom-select";
import 'bootstrap/dist/js/bootstrap.bundle';
import {MounDatatable} from "./moun_datatable/MounDatatable";
import {MounTomSelect} from "./MounTomSelect";
import "sweetalert2"
import Swal from "sweetalert2";
import {displayToast} from "./toast";

const btns = document.querySelectorAll('button[data-url]');
btns.forEach(btn => {
    btn.onclick = async (e) => {
        e.preventDefault();

        const url = btn.dataset.url;
        const response = await fetch(url);
        const result = await response.text();

        Swal.fire({
            html: `${result}`,
            showCloseButton: true,
            showConfirmButton: false,
            showClass: {
                popup: 'animate__animated animate__fadeIn animate__faster',
            },
            hideClass: {
                popup: 'animate__animated animate__fadeOut animate__faster',
            },
            didOpen: () => {
                const selects = Swal.getContainer().querySelectorAll('select:not(#swal2-select.swal2-select)');
                new MounTomSelect(selects);
                Swal.getHtmlContainer().style.overflow = 'visible';
            }
        })

        const submitButton = document.querySelector('button.ajax')
        if (submitButton) {
            submitButton.onclick = async (e) => {
                e.preventDefault()
                const htmlForm = submitButton.closest('form');
                const formData = new FormData(htmlForm);

                const response = await fetch(htmlForm.action, {
                    method: 'POST',
                    body: formData
                })

                if (!response.ok) {
                    displayToast('error', 'Une erreur est survenue...');
                    Swal.close();
                    return;
                }

                const result = await response.json();

                const categoryBadges = document.querySelectorAll(`.operation-${result.data.operation_id} .badge.category`)

                Array.from(categoryBadges).forEach(categoryBadge => {
                    categoryBadge.textContent = result.data.category_name;
                    categoryBadge.setAttribute('style', `background-color: ${result.data.category_color}`)
                })


                displayToast('success', 'Catégorie modifiée avec succès');
                Swal.close();
            }
        }
    }
})


const selects = document.querySelectorAll('select');

new MounTomSelect(selects);

const computeRemainingMonthButton = document.querySelector('#compute-remaining-month');

if (computeRemainingMonthButton) {
    computeRemainingMonthButton.onclick = async (e) => {
        e.preventDefault();

        const url = computeRemainingMonthButton.dataset.urlCompute;
        const response = await fetch(url);
        const result = await response.text();
        Swal.fire({
            html: `${result}`,
            showCloseButton: true,
            showConfirmButton: false,
            showClass: {
                popup: 'animate__animated animate__fadeIn animate__faster',
            },
            hideClass: {
                popup: 'animate__animated animate__fadeOut animate__faster',
            },
        })
        const submitButton = document.querySelector('#submit-compute-remaining-month');
        submitButton.onclick = (e) => {
            e.preventDefault();
            const inputComputeRemainingMonths = document.querySelector('#input-compute-remaining-months');
            const remainingAmount = Number(inputComputeRemainingMonths.dataset.remainingAmount);
            const value = Number(inputComputeRemainingMonths.value);

            const remainingMonths = Math.ceil(remainingAmount / value);

            const divToSetValueComputed = document.querySelector('#display-remaining-months');
            divToSetValueComputed.textContent = String(remainingMonths) + ' mois';
        }
    }
}
