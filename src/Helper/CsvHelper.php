<?php

namespace App\Helper;

class CsvHelper
{
    public static function getCsvHeader(string $filePath, string $separator, $offsetHeaderRow = 1): bool|array
    {
        $filestream = fopen($filePath, "r");
        $rowNumber = 1;
        $header = [];

        while ($rowData = fgetcsv($filestream, null, $separator)) {
            if ($rowNumber === $offsetHeaderRow) {
                $header = $rowData;
                break;
            }

            $rowNumber++;
        }

        $header = array_map(function ($column) {
            $column = strtolower($column);
            $column = utf8_encode($column);
            $column = str_replace('é', 'e', $column);
            return str_replace(' ', '_', $column);
        }, $header);

        return array_filter($header);
    }

    public static function getCsvHeaderFormattedForColumnTable(string $filename, string $separator): array
    {
        $filestream = fopen($filename, "r");
        $columns = fgetcsv($filestream, null, $separator);
        return array_map(function($column) {
            $column = strtolower($column);
            return (string) preg_replace_callback_array([
                '/-|\(|\)|\'| /' => function() {
                    return '_';
                },
                '/é|è|ê/' => function() {
                    return 'e';
                },
                '/à|â/' => function() {
                    return 'a';
                },
                '/ô/' => function() {
                    return 'o';
                },
                '/û/' => function() {
                    return 'u';
                },
                '/ç/' => function() {
                    return 'c';
                },
                '/\.csv$|\//' => function() {
                    return '';
                }
            ], $column);
        },$columns);
    }

    public static function getCsvContent(string $filename, string $separator): array
    {
        $filestream = fopen($filename, "r");

        $content = [];
        $rowNumber = 1;

        while ($rowData = fgetcsv($filestream, null, $separator)) {
            if ($rowNumber > 1) {
                foreach ($rowData as $colData) {
                    $content[$rowNumber][] = $colData;
                }
            }

            $rowNumber++;
        }

        return array_values($content);
    }

    public static function getCsvContentWithColumnKeys(string $filePath, string $separator, int $offsetHeaderRow = 1, int $offsetContentRow = 2): array
    {
        $filestream = fopen($filePath, "r");

        $header = self::getCsvHeader($filePath, $separator, $offsetHeaderRow);
        $content = [];
        $rowNumber = 1;
        $k = 0;
        while ($rowData = fgetcsv($filestream, null, $separator)) {
            if ($rowNumber >= $offsetContentRow) {
                foreach ($rowData as $colData) {
                    if($k < 4) {
                        $content[$rowNumber][$header[$k]] = $colData;
                    }
                    $k++;
                }
                $k = 0;
            }

            $rowNumber++;
        }

        return array_values($content);
    }
}
