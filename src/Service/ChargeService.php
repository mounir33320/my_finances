<?php

namespace App\Service;

use App\Entity\Charge;
use App\Entity\User;
use App\Repository\ChargeRepository;
use Doctrine\Common\Collections\Criteria;
use RuntimeException;
use Symfony\Component\Security\Core\User\UserInterface;

class ChargeService
{
    public function __construct(private readonly ChargeRepository $chargeRepository)
    {
    }

    public function getChargesByUser(UserInterface $user): array
    {
        $this->verifyUser($user);

        return $this->chargeRepository->findBy(['user' => $user], ['directDebitDay' => Criteria::ASC]);
    }

    public function save(Charge $charge, ?UserInterface $user): Charge
    {
        /** @var User $user */
        $this->verifyUser($user);

        $user->addCharge($charge);
        $this->chargeRepository->save($charge, true);
        return $charge;
    }

    public function update(Charge $charge): Charge
    {
        $this->chargeRepository->update($charge);

        return $charge;
    }

    public function delete(Charge $charge): bool
    {
        $this->chargeRepository->remove($charge, true);
        return true;
    }

    private function verifyUser(UserInterface $user): void
    {
        if (!$user instanceof User) {
            throw new RuntimeException('user must be an instance of User entity');
        }
    }
}
