<?php

namespace App\Service\OperationService;

use App\Entity\OperationCategory;
use App\Entity\User;
use App\Repository\OperationCategoryRepository;
use App\Traits\UserVerifyTrait;
use Symfony\Component\Security\Core\User\UserInterface;

class OperationCategoryService
{
    use UserVerifyTrait;

    public function __construct(private readonly OperationCategoryRepository $operationCategoryRepository)
    {
    }

    public function getCategoriesByUser(UserInterface $user): array
    {
        return $this->operationCategoryRepository->findBy(['user' => $user]);
    }

    public function save(OperationCategory $category, UserInterface $user): OperationCategory
    {
        /** @var User $user */
        $this->verifyUser($user);

        $user->addOperationCategory($category);
        $this->operationCategoryRepository->save($category, true);

        return $category;
    }

    public function update(OperationCategory $category): OperationCategory
    {
        $this->operationCategoryRepository->update($category);

        return $category;
    }

    public function delete(OperationCategory $category): bool
    {
        $this->operationCategoryRepository->remove($category, true);
        return true;
    }
}
