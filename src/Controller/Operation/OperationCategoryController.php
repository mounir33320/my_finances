<?php

namespace App\Controller\Operation;

use App\Entity\OperationCategory;
use App\Form\OperationType\OperationCategoryType;
use App\Service\OperationService\OperationCategoryService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/operation-categories', name: 'operation-categories_')]
class OperationCategoryController extends AbstractController
{
    public function __construct(private readonly OperationCategoryService $operationCategoryService)
    {
    }

    #[Route('/', name: 'index')]
    public function index(Request $request): Response
    {
        $categories = $this->operationCategoryService->getCategoriesByUser($this->getUser());

        return $this->render('operations/operation_category/index.html.twig', [
            'categories' => $categories,
        ]);
    }

    #[Route('/add/', name: 'add')]
    public function add( Request $request): RedirectResponse|Response
    {
        $category = new OperationCategory();
        $form = $this->createForm(OperationCategoryType::class, $category, ['action' => $request->getRequestUri()]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->operationCategoryService->save($category, $this->getUser());
            $this->addFlash('success', 'Catégorie modifiée avec succès');

            return $this->redirectToRoute('operation-categories_index');
        }

        return $this->render('operations/operation_category/form.html.twig', [
            'form' => $form->createView(),
            'title' => 'Ajouter une catégorie',
            'submitButtonName' => 'Ajouter'
        ]);
    }

    #[Route('/edit/{id}', name: 'edit')]
    public function edit(OperationCategory $category, Request $request): RedirectResponse|Response
    {
        $form = $this->createForm(OperationCategoryType::class, $category, ['action' => $request->getRequestUri()]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->operationCategoryService->update($category);
            $this->addFlash('success', 'Catégorie modifiée avec succès');

            return $this->redirectToRoute('operation-categories_index');
        }

        return $this->render('operations/operation_category/form.html.twig', [
            'form' => $form->createView(),
            'title' => 'Éditer une catégorie',
            'submitButtonName' => 'Modifier'
        ]);
    }

    #[Route('/delete/{id}', name: 'delete')]
    public function delete(Request $request, OperationCategory $category): RedirectResponse
    {
        $token = $request->get('_token');

        if ($this->isCsrfTokenValid('delete'.$category->getId(),$token)) {
            $this->operationCategoryService->delete($category);
            $this->addFlash('success', 'Catégorie supprimée avec succès');
            return $this->redirect($request->headers->get('referer'));
        }

        $this->addFlash('error', 'Une erreur est survenue');
        return $this->redirect($request->headers->get('referer'));
    }
}
