<?php

namespace App\CsvReader;

use App\Entity\OperationFile;
use App\Helper\CsvHelper;

class CreditAgricoleCsvReader
{
    public static function getHeader(OperationFile $operationFile): bool|array
    {
        return CsvHelper::getCsvHeader($operationFile->getFilePath(), ';', 11);
    }

    public static function getContent(OperationFile $operationFile): array
    {
        return CsvHelper::getCsvContentWithColumnKeys($operationFile->getFilePath(), ';', 11, 12);
    }
}
