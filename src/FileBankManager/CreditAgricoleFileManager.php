<?php

namespace App\FileBankManager;

use App\CsvReader\CreditAgricoleCsvReader;
use App\Entity\Operation;
use App\Entity\OperationCategory;
use App\Entity\OperationFile;
use App\Entity\OperationPattern;
use App\Entity\User;
use App\Repository\OperationCategoryRepository;
use App\Repository\OperationPatternRepository;
use App\Repository\OperationRepository;

class CreditAgricoleFileManager implements FileBankManagerInterface
{
    public const BANK_NAME = 'Crédit Agricole';
    public function __construct(
        private readonly OperationRepository $operationRepository,
        private readonly OperationCategoryRepository $operationCategoryRepository,
        private readonly OperationPatternRepository $operationPatternRepository)
    {
    }

    public function manageFile(OperationFile $operationFile, User $user): void
    {
        $content = CreditAgricoleCsvReader::getContent($operationFile);
        $patterns = $this->operationPatternRepository->findBy(['user' => $user]);
        $noCategory = $this->operationCategoryRepository->findOneBy(['name' => 'Non catégorisé']);
        if (!$noCategory) {
            $noCategory = (new OperationCategory())
                ->setUser($user)
                ->setColor('#34495e')
                ->setName('Non catégorisé')
            ;
            $this->operationCategoryRepository->save($noCategory, true);
        }

        foreach ($content as $operationRow) {
            $date = \DateTime::createFromFormat('d/m/Y', $operationRow['date']);

            if (!$date) {
                continue;
            }

            $label = $this->generateFormattedLabel($operationRow['libelle']);
            $label = mb_convert_encoding($label, 'UTF-8', 'ISO-8859-1');
            $paymentType = $this->getPaymentType($operationRow['libelle']);
            $credit = $operationRow['credit_euros'];
            $debit = $operationRow['debit_euros'];
            $amount = !empty($credit) ? $this->formatStringToFloat($credit) : -$this->formatStringToFloat($debit);

            $operationId = implode('-',[$date->format('d-m-Y'),$label,$paymentType, (string) $amount]);
            $operationId = preg_replace('/\n| /', '', $operationId);

            $existingOperation = $this->operationRepository->findOneBy(['operationId' => $operationId, 'user' => $user]);

            if ($existingOperation) {
                continue;
            }

            $category = $this->getCategory($label, $patterns);

            $operation = (new Operation())
                ->setUser($user)
                ->setAmount($amount)
                ->setLabel($label)
                ->setDate($date)
                ->setCategory($category ?? $noCategory)
                ->setPaymentType($paymentType)
                ->setOperationId($operationId)
            ;

            $this->operationRepository->save($operation);
        }

        $this->operationRepository->flush();
    }


    public function supports(string $bankName): bool
    {
        return $bankName === self::BANK_NAME;
    }

    private function generateFormattedLabel(string $label): string
    {
        $regex = '/';
        $regex .= 'paiement par carte *\n';
        $regex .= '|virement emis *\n';
        $regex .= '|virement en votre faveur *\n';
        $regex .= '|prelevement *\n';
        $regex .= '|cheque *\n';
        $regex .= '|cotisation *\n';
        $regex .= '|retrait au distributeur *\n';
        $regex .= '/i';

        return preg_replace($regex, '', $label);
    }
    private function getPaymentType(string $label): ?string
    {
        switch ($label) {
            case str_contains(strtolower($label), 'paiement par carte'):
                return Operation::PAYMENT_CARD;

            case str_contains(strtolower($label), 'virement'):
                return Operation::PAYMENT_BANK_TRANSFER;

            case str_contains(strtolower($label), 'prelevement'):
                return Operation::PAYMENT_DIRECT_DEBIT;

            case str_contains(strtolower($label), 'cheque'):
                return Operation::PAYMENT_CHEQUE;

            case str_contains(strtolower($label), 'cotisation'):
                return Operation::PAYMENT_SUBSCRIPTION;

            case str_contains(strtolower($label), 'retrait au distributeur'):
                return Operation::PAYMENT_WITHDRAWING_CASH;

            default: return 'autre';
        }
    }

    private function formatStringToFloat(string $number): float
    {
        $number = mb_convert_encoding($number, 'UTF-8');
        $number = str_replace(',', '.', $number);
        $number = str_replace('?', '', $number);

        return (float) $number;
    }

    public function getCategory(string $label, array $patterns): ?OperationCategory
    {
        $pattern = array_filter($patterns, function (OperationPattern $pattern) use ($label) {
            $patternString = $pattern->getPattern();
            $patternExploded = explode(' ', $patternString);

            foreach ($patternExploded as $patternString) {
                if (!str_contains(strtolower($label), strtolower($patternString))) {
                    return false;
                }
            }

            return true;
        });

        return array_values($pattern) ? array_values($pattern)[0]->getCategory() : null;
    }
}
