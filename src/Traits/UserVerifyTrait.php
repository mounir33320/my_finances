<?php

namespace App\Traits;

use App\Entity\User;
use RuntimeException;
use Symfony\Component\Security\Core\User\UserInterface;

trait UserVerifyTrait
{
    private function verifyUser(UserInterface $user): void
    {
        if (!$user instanceof User) {
            throw new RuntimeException('user must be an instance of User entity');
        }
    }
}
