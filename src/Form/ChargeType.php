<?php

namespace App\Form;

use App\Entity\Charge;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChargeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Titre'
            ])
            ->add('amount', MoneyType::class, [
                'label' => 'Montant'
            ])
            ->add('directDebitDay', NumberType::class, [
                'label' => 'Jour du prélèvement',
                'required' => false
            ])
        ;

        if (isset($options['action'])) {
            $builder->setAction($options['action']);
        }

        $builder->get('directDebitDay')->addEventListener(FormEvents::PRE_SUBMIT, function(FormEvent $event) {
            $directDebitDay = $event->getData();

            if ((int) $directDebitDay > 31 || (int) $directDebitDay < 1) {
                $directDebitDay = null;
            }

            $event->setData($directDebitDay);
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Charge::class,
            'attr'       => [
                'autocomplete' => 'off'
            ]
        ]);
    }
}
