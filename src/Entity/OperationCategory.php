<?php

namespace App\Entity;

use App\Repository\OperationCategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Entity(repositoryClass: OperationCategoryRepository::class)]
class OperationCategory
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 100)]
    private ?string $name = null;

    #[ORM\Column(length: 100, nullable: true)]
    private ?string $color = null;

    #[ORM\OneToMany(mappedBy: 'category', targetEntity: OperationPattern::class)]
    private Collection $operationPatterns;

    #[ORM\OneToMany(mappedBy: 'category', targetEntity: Operation::class)]
    private Collection $operations;

    #[ORM\ManyToOne(inversedBy: 'operationCategories')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;

    #[ORM\Column(length: 100, nullable: true)]
    private ?string $icon = null;

    public function __construct()
    {
        $this->operationPatterns = new ArrayCollection();
        $this->operations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @return Collection<int, OperationPattern>
     */
    public function getOperationPatterns(): Collection
    {
        return $this->operationPatterns;
    }

    public function addOperationPattern(OperationPattern $operationPattern): self
    {
        if (!$this->operationPatterns->contains($operationPattern)) {
            $this->operationPatterns->add($operationPattern);
            $operationPattern->setCategory($this);
        }

        return $this;
    }

    public function removeOperationPattern(OperationPattern $operationPattern): self
    {
        if ($this->operationPatterns->removeElement($operationPattern)) {
            // set the owning side to null (unless already changed)
            if ($operationPattern->getCategory() === $this) {
                $operationPattern->setCategory(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Operation>
     */
    public function getOperations(): Collection
    {
        return $this->operations;
    }

    public function addOperation(Operation $operation): self
    {
        if (!$this->operations->contains($operation)) {
            $this->operations->add($operation);
            $operation->setCategory($this);
        }

        return $this;
    }

    public function removeOperation(Operation $operation): self
    {
        if ($this->operations->removeElement($operation)) {
            // set the owning side to null (unless already changed)
            if ($operation->getCategory() === $this) {
                $operation->setCategory(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(?string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }
}
