<?php

namespace App\Service;

use App\DTO\DebtDto;
use App\DTO\DebtListDto;
use App\Entity\Debt;
use App\Entity\Payment;
use App\Entity\User;
use App\Repository\DebtRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class DebtService
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly DebtRepository $debtRepository,
        private readonly UrlGeneratorInterface $urlGenerator
    ) { }

    public function create(UserInterface $user, Debt $debt): Debt
    {
        if (!$user instanceof User) {
            throw new \Exception('The user must be an instance of User entity');
        }

        $user->addDebt($debt);

        $this->entityManager->persist($debt);
        $this->entityManager->flush();

        return $debt;
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function getDebtListByUser(UserInterface $user): ?DebtListDto
    {
        if (!$user instanceof User) {
            throw new \Exception('The user must be an instance of User entity');
        }

        $debts = $this->debtRepository->findBy(['user' => $user]);

        if (empty($debts)) {
            return null;
        }

        $totalAmount = array_reduce($debts, function ($carry, Debt $debt) {
            $carry += $debt->getAmount();
            return $carry;
        });
        $debtsListDTO = new DebtListDto();
        $debtsListDTO->totalAmount = $totalAmount ?? 0;

        foreach ($debts as $debt) {
            $debtDto = $this->convertDebtToDto($debt, $totalAmount);

            $debtsListDTO->debtsDTO[] = $debtDto;
            $debtsListDTO->alreadyPaid += $debtDto->alreadyPaid;
            $debtsListDTO->remainingAmount += $debtDto->remainingAmount;
        }

        $debtsListDTO->alreadyPaidPercent = round($debtsListDTO->alreadyPaid / $debtsListDTO->totalAmount * 100);
        $debtsListDTO->remainingAmountPercent = round($debtsListDTO->remainingAmount / $debtsListDTO->totalAmount * 100);

        return $debtsListDTO;
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function convertDebtToDto(Debt $debt, int $totalAmount = null): DebtDto
    {
        if ($totalAmount === null) {
           $totalAmount = $this->debtRepository->calculateTotalAmountOfDebt();
        }

        $debtDto = new DebtDto();
//        $debtDto->payments = $debt->getPayments()->toArray();
        $debtDto->payments = $debt->getPayments()->matching((new Criteria())->orderBy(['createdAt' => Criteria::DESC]))->toArray();
        $debtDto->amount = $debt->getAmount();
        $debtDto->creditor = $debt->getCreditor();
        $debtDto->alreadyPaid = array_reduce($debt->getPayments()->toArray(), function ($carry, Payment $payment) {
            $carry += $payment->getAmount();
            return $carry;
        }) ?? 0;
        $debtDto->alreadyPaidPercent = round($debtDto->alreadyPaid / $debt->getAmount() * 100);
        $debtDto->amountPercentAboutAllDebts = $debt->getAmount() / $totalAmount * 100;
        $debtDto->remainingAmount = $debt->getAmount() - $debtDto->alreadyPaid;
        $debtDto->remainingAmountPercent = round(($debt->getAmount() - $debtDto->alreadyPaid) / $debt->getAmount() * 100);
        $debtDto->url = $this->urlGenerator->generate('debts_show', ['id' => $debt->getId()]);
        $debtDto->removeUrl = $this->urlGenerator->generate('debts_delete', ['id' => $debt->getId()]);
        $debtDto->id = $debt->getId();

        return $debtDto;
    }

    public function delete(Debt $debt): void
    {
        $this->debtRepository->remove($debt, true);
    }
}
