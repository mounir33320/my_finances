<?php

namespace App\Controller\Operation;

use App\Exception\FileBankManagerNotFoundException;
use App\Exception\FileBankNotFoundException;
use App\Form\OperationType\OperationUploadFileType;
use App\Service\OperationService\OperationFileService;
use App\Service\OperationService\OperationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/operation-file',name: 'operation-file_')]
class OperationFileController extends AbstractController
{
    public function __construct(private readonly OperationFileService $operationFileService, private readonly OperationService $operationService)
    {
    }

    /**
     * @throws FileBankNotFoundException
     * @throws FileBankManagerNotFoundException
     */
    #[Route('/', name: 'index')]
    public function index(Request $request): RedirectResponse|Response
    {
        $form = $this->createForm(OperationUploadFileType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if (!$form->isValid()) {
                $errors = '';
                foreach ($form->getErrors(true) as $error) {
                    $errors .= $error->getMessage(). "\n";
                }
                $this->addFlash('error', $errors);

                return $this->redirectToRoute('operations_index');
            }

            $file = $form['file']->getData();
            $operationFile = $this->operationFileService->save($file, $this->getUser());
            $this->operationService->saveOperationsByOperationFile($operationFile, $this->getUser());
            $this->addFlash('success', 'Les opérations ont bien été importées');

            return $this->redirectToRoute('operations_index');
        }

        return $this->render('operations/operation_file/index.html.twig',[
            'form' => $form->createView(),
            'title' => 'Uploader un fichier'
        ]);
    }
}
