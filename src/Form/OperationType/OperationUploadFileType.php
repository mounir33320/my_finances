<?php

namespace App\Form\OperationType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\Constraints\File;

class OperationUploadFileType extends AbstractType
{
    public function __construct(private readonly UrlGeneratorInterface $urlGenerator)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('file', FileType::class, [
                'label' => 'Fichier csv',
                'constraints' => [
                    new File([
                        'maxSize' => '10M',
                        'extensions' => [
                            'csv'=> [
                                'application/vnd.ms-excel',
                                'text/csv',
                                'text/plain'
                            ]
                        ],
                        'extensionsMessage' => 'Veuillez uploader un fichier CSV',
//                        'maxSizeMessage' => 'Veuillez uploader un fichier inférieur à 10 Mo',
//                        'mimeTypes' => [
//                            'application/vnd.ms-excel',
//                            'text/csv',
//                        ],
//                        'mimeTypesMessage' => 'Veuillez uploader un fichier CSV',
                    ])
                ],
                'attr' => [
                    'accept' => 'application/vnd.ms-excel, text/csv'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'action' => $this->urlGenerator->generate('operation-file_index')
        ]);
    }
}
