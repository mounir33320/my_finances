<?php

namespace App\Form\OperationType;

use App\Entity\OperationCategory;
use App\Model\OperationFilter;
use App\Repository\OperationCategoryRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class OperationFilterType extends AbstractType
{
    public function __construct(
        private readonly TokenStorageInterface $storage,
        private readonly OperationCategoryRepository $operationCategoryRepository
    )
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('dateFrom', DateType::class, [
                'label' => 'À partir de',
                'widget' => 'single_text',
                'input' => 'datetime',
                'required' => false
            ])
            ->add('dateTo', DateType::class, [
                'label' => 'Jusqu\'à',
                'widget' => 'single_text',
                'input' => 'datetime',
                'required' => false
            ])
            ->add('categories', EntityType::class, [
                'label' => 'Catégories',
                'class' => OperationCategory::class,
                'choice_label' => 'name',
                'multiple' => true,
                'required' => false,
                'choices' => $this->getCategories(),
                'group_by' => null
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => OperationFilter::class,
            'method' => 'GET',
            'csrf_protection' => false
        ]);
    }

    public function getCategories(): iterable
    {
        $categories = $this->operationCategoryRepository->findBy(['user' => $this->storage->getToken()->getUser()]);
        $noCategory = array_filter($categories, function(OperationCategory $value) {
            return $value->getName() === 'Non catégorisé';
        });

        if (!empty($noCategory)) {
            unset($categories[array_key_first($noCategory)]);
            array_unshift($categories, array_values($noCategory)[0]);
        }

        return $categories;
    }
}
