<?php

namespace App\Controller;

use App\Entity\Charge;
use App\Form\ChargeType;
use App\Model\ChargesInfo;
use App\Service\ChargeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/charges', name: 'charge_')]
class ChargeController extends AbstractController
{
    public function __construct(private readonly ChargeService $chargeService)
    {
    }

    #[Route('/', name: 'index')]
    public function index(): Response
    {
        $charges = $this->chargeService->getChargesByUser($this->getUser());
        $chargesInfo = $this->getUser()->getSalary() > 0 ?  new ChargesInfo($charges) : null;

        return $this->render('charge/index.html.twig', [
            'charges' => $charges,
            'chargesInfo' => $chargesInfo,
        ]);
    }

    #[Route('/add', name: 'add')]
    public function add(Request $request): RedirectResponse|Response
    {
        $charge = new Charge();
        $form = $this->createForm(ChargeType::class, $charge, ['action' => $request->getRequestUri()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->chargeService->save($charge, $this->getUser());
            $this->addFlash('success', 'Charge ajoutée avec succès');

            return $this->redirectToRoute('charge_index');
        }

        return $this->render('charge/form.html.twig', [
            'title' => 'Ajouter une charge',
            'form' => $form->createView(),
            'submitButtonName' => 'Ajouter'
        ]);
    }

    #[Route('/edit/{id}', name: 'edit')]
    public function edit(Request $request, Charge $charge): RedirectResponse|Response
    {
        $form = $this->createForm(ChargeType::class, $charge, ['action' => $request->getRequestUri()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->chargeService->update($charge);
            $this->addFlash('success', 'Charge modifiée avec succès');

            return $this->redirectToRoute('charge_index');
        }

        return $this->render('charge/form.html.twig', [
            'title' => 'Éditer une charge',
            'form' => $form->createView(),
            'submitButtonName' => 'Modifier'
            ]);
    }

    #[Route('/delete/{id}', name: 'delete')]
    public function delete(Request $request, Charge $charge): RedirectResponse
    {
        $token = $request->get('_token');

        if ($this->isCsrfTokenValid('delete'.$charge->getId(),$token)) {
            $this->chargeService->delete($charge);
            $this->addFlash('success', 'Charge supprimée avec succès');
            return $this->redirect($request->headers->get('referer'));
        }

        $this->addFlash('error', 'Une erreur est survenue');
        return $this->redirect($request->headers->get('referer'));
    }
}
