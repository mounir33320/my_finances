<?php

namespace App\Service\OperationService;

use App\Entity\OperationPattern;
use App\Entity\User;
use App\Repository\OperationPatternRepository;
use App\Traits\UserVerifyTrait;
use Symfony\Component\Security\Core\User\UserInterface;

class OperationPatternService
{
    use UserVerifyTrait;
    public function __construct(private readonly OperationPatternRepository $operationPatternRepository)
    {
    }

    public function getPatternsByUser(UserInterface $user)
    {
        /** @var User */
        $this->verifyUser($user);

        return $this->operationPatternRepository->findBy(['user' => $user]);
    }

    public function save(OperationPattern $pattern, UserInterface $user): OperationPattern
    {
        /** @var User $user */
        $this->verifyUser($user);

        $user->addOperationPattern($pattern);
        $this->operationPatternRepository->save($pattern, true);

        return $pattern;
    }

    public function update(OperationPattern $pattern): OperationPattern
    {
        $this->operationPatternRepository->update($pattern);

        return $pattern;
    }

    public function delete(OperationPattern $pattern): bool
    {
        $this->operationPatternRepository->remove($pattern, true);
        return true;
    }
}
