<?php

namespace App\DTO;

use Doctrine\Common\Collections\Collection;

class DebtListDto
{
    public float $totalAmount = 0;
    public array $debtsDTO = [];
    public float $alreadyPaid = 0;
    public float $alreadyPaidPercent = 0;
    public float $remainingAmount = 0;
    public float $remainingAmountPercent = 0;
}
