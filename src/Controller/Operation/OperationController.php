<?php

namespace App\Controller\Operation;

use App\Entity\Operation;
use App\Form\OperationType\OperationFilterType;
use App\Form\OperationType\OperationType;
use App\Form\OperationType\OperationUploadFileType;
use App\Model\OperationFilter;
use App\Service\OperationService\OperationFileService;
use App\Service\OperationService\OperationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

#[Route('/operations', name: 'operations_')]
class OperationController extends AbstractController
{
    public function __construct(private readonly OperationService $operationService, private OperationFileService $operationUploadFileService)
    {
    }

    #[Route('/', name: 'index')]
    public function index(Request $request): Response
    {
        $form = $this->createForm(OperationUploadFileType::class);

        $operationFilter = new OperationFilter();
        $filterForm = $this->createForm(OperationFilterType::class, $operationFilter);
        $filterForm->handleRequest($request);

        $operations = $this->operationService->getAll($operationFilter, $this->getUser());

        $operationInfo = $this->operationService->getOperationInfo($operations);
        return $this->render('operations/operation/index.html.twig', [
            'operations' => $operations,
            'operationInfo' => $operationInfo,
            'form' => $form->createView(),
            'filterForm' => $filterForm
        ]);
    }

    #[Route('/edit/{id}', name: 'edit')]
    public function edit(Operation $operation, Request $request, NormalizerInterface $normalizer): RedirectResponse|Response
    {
        $form = $this->createForm(OperationType::class, $operation, ['action' => $request->getRequestUri()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->operationService->update($operation);

            return new JsonResponse(['success' => true, 'data' => [
                'operation_id' => $operation->getId(),
                'category_id' => $operation->getCategory()->getId(),
                'category_name' => $operation->getCategory()->getName(),
                'category_color' => $operation->getCategory()->getColor()
            ]]);
        }

        return $this->render('operations/operation/edit.html.twig', [
            'form' => $form->createView(),
            'title' => 'Éditer la catégorie',
            'submitMode' => 'ajax'
        ]);
    }

    #[Route('/delete-all-operations', name: 'delete_all_operations')]
    public function deleteAllOperations(Request $request): RedirectResponse
    {
        $token = $request->get('_token');

        if ($this->isCsrfTokenValid('delete_all_operations', $token)) {
            $this->operationService->deleteAllOperations($this->getUser());
            $this->addFlash('success', 'Toutes les opérations ont bien été supprimées');

        } else {
            $this->addFlash('error', 'Une erreur est survenue...');
        }

        return $this->redirectToRoute('operations_index');
    }
}
